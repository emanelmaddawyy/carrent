import React, { Component } from 'react';
import '../stylesheet/App.css';
import OfficeInfo from './OfficeInfo';
import CarList from './CarList';
import AlertDismissable from './alert'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      Cars: [
        {
          maker: "American Car",
          model: "Tiger Shark Show Rod ",
          color: "red",
          number: 1111,
          avalibale: true

        },
        {
          maker: "Mitsubishi",
          model: "Lancer",
          color: "white",
          number: 2222,
          avalibale: false

        },
        {
          maker: "Chevrolet",
          model: "Optra",
          color: "Black",
          number: 2233,
          avalibale: false

        }
      ]
    }
  }

  getCarsNumber() {
    return this.state.Cars.length
  }

  getAvailableCarsCount() {
    // let availableCount = 0
    // this.state.Cars.map((item, i) => {
    //   if (item.avalibale) {
    //     availableCount++
    //   }
    // })

    // return availableCount

    let filteredCars = this.state.Cars.filter((item) => {
      return item.avalibale
    })

    return filteredCars.length
  }

  render() {
    return (
      <div className="App-header">
        <OfficeInfo name={"car Rent"} address={"Cairo"} number={this.getCarsNumber()} avalibaleCarsNo={this.getAvailableCarsCount()} />
        <div className="carlist">
          <CarList Cars={this.state.Cars} />

        </div>
        <AlertDismissable />
      </div>

    
    );
  }
    

}



export default App;
