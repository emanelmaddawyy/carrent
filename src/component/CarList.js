import React, { Component } from 'react'
import CarRow from './CarRow'

class CarList extends Component {
    render() {
        return (
            <div>
                <table>
                    <thead>
                        <tr> <th>Car Maker</th>
                            <th>Car Model </th>
                            <th>Color </th>
                            <th>Car Number</th>
                            <th>Is Avaliable</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.Cars.map((item, i) =>
                                <CarRow maker={item.maker} model={item.model} color={item.color} number={item.number} avalibale={item.avalibale} />
                            )
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
export default CarList