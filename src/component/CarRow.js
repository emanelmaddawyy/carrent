import React, { Component } from 'react'
import PropTypes from 'prop-types'

class CarRow extends Component {
    render() {
        return (
            <tr>
                <td> {this.props.maker}</td>
                <td> {this.props.model}</td>
                <td> {this.props.color}</td>
                <td> {this.props.number}</td>
                <td> {(this.props.avalabile) ? "Available" : "Not Available"}</td>

            </tr>
        )
    }
}

CarRow.propTypes = {
    number: PropTypes.string
}

export default CarRow