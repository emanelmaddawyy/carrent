import React, { Component } from 'react'

class OfficeInfo extends Component {
    render() {
        return (
            <div>
                <h1> Offic Name : </h1>
                <span>{this.props.name}</span>
                <h1> Office Address : </h1>
                <span>{this.props.address} </span>
                <h1> Office Number : </h1>
                <span> {this.props.number} </span>
                <h1> Available Cars Number : </h1>
                <span> {this.props.avalibaleCarsNo} </span>
            </div>
        )
    }
}
export default OfficeInfo